/*
Write a function called repeatString which repeats the given String src exactly count times.
#Examples:
repeatStr(6, "I") // "IIIIII"
repeatStr(5, "Hello") // "HelloHelloHelloHelloHello"
*/

function repeatStr (n, s) {
    var output = "";

    for (i = 0; i <= n; i++) {
        output += s;
    }
    return output;
}

// Test

describe("Tests", function() {
    it ("Basic tests", function() {
        Test.assertSimilar(repeatStr(3, "*"), "***");
        Test.assertSimilar(repeatStr(5, "#"), "#####");
        Test.assertSimilar(repeatStr(2, "ha "), "ha ha ");
    });
});

// Best Practice !!

function repeatStr (n, s) {
    return s.repeat(n);
}