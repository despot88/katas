/*
Sum all the numbers of the array (in F# and Haskell you get a list) except the highest and the lowest element (the value, not the index!).
(The highest/lowest element is respectively only one element at each edge, even if there are more than one with the same value!)
If array is empty, null or None, or if only 1 Element exists, return 0.
#Examples:
{ 6, 2, 1, 8, 10 } => 16
{ 1, 1, 11, 2, 3 } => 6
*/

function sumArray(array) {
    let arr = array;

    if(arr == null || arr == 0 || arr == []) {
        return 0;
    }

    let sum = arr.reduce((a, b) => a + b, 0);
    return sum - Math.min(...arr) - Math.max(...arr);
}

// 1 test failed

// Test

Test.assertEquals(sumArray([ 6, 2, 1, 8, 10 ]), 16);

// Best Practice !!

