/*
Your program must find the longest sequence of consecutive zeroes in an integer number.
For example, the number 10002030000 has three sequences of zeroes with lengths 3, 1 and 4. So the longest sequence is 4.
Input :
A positive integer
Output :
The max number of consecutive zeros in that number
Restrictions :
Do it in less than 60 (59 or less) chars.
Note :
The number will always have 1 or more zeroes
*/

// str.match(/xxx/g); gi = case-insensitive

function f(number) {
    const zeros = number.toString().match(/0+/g);
    const zerosLenght = zeros.map(z => z.length);
    const longestZeros = Math.max(...zerosLenght);
    return longestZeros;
}


//Short < 59 chars
f=n=>Math.max(...(''+n).match(/0+/g).map(z=>z.length))

// Test
describe('Sample', () => {
    it('Tests', () => {
        Test.assertEquals(f(1002000), 3)
        Test.assertEquals(f(10002030000), 4)
        Test.assertEquals(f(10), 1)
    });
});

// Best Practice
f=Q=>/(0+)(?!.*\1)/.exec(Q)[1].length