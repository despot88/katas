/*
Given a random number:
You have to return the digits of this number within an array in reverse order.
Example:
348597 => [7,9,5,8,4,3]
*/

function digitize(n) {
    let numArr = n.toString();
    let arrSplit = numArr.split("");
    let arrReverse = arrSplit.reverse();

    for (i = 0; i < arrReverse.length; i++) {
        arrReverse[i] = parseInt(arrReverse[i]);
    }

    return arrReverse;
}

// Test
Test.assertDeepEquals(digitize(35231),[1,3,2,5,3]);

// Best Practice
function digitize(n) {
    return String(n).split('').map(Number).reverse()
}


